var table1;
var table2;
var table3;

function fillPanels() {
    $.ajax({
        type: "POST",
        url: 'php/getAllData.php',
        dataType: "json",
        success: function(data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                $('#table1').append('<tr><td>' + data[i].name + '</td><td>' + data[i].phone + '</td></tr>');
                $('#table2').append('<tr><td>' + data[i].name + '</td><td>' + data[i].adress + '</td></tr>');
                $('#table3').append('<tr><td>' + data[i].name + '</td><td>' + data[i].city + '</td></tr>');

            }
            table1 = $('#table1').DataTable({
                "lengthChange": false,
                "info": false,
                "bDestroy": true
            });
            table2 = $('#table2').DataTable({
                "lengthChange": false,
                "info": false,
                "bDestroy": true
            });
            table3 = $('#table3').DataTable({
                "lengthChange": false,
                "info": false,
                "bDestroy": true
            });
        },
        error: function(request, status, error) {
            alert(request.responseText);
        }
    });
}

function formValidation() {

    $("form[name='insertData']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: {
                required: true,
                maxlength: 30
            },
            tel: {
                required: true,
                digits: true
            },
            adress: {
                required: true,
                maxlength: 30
            },
            city: {
                required: true,
                maxlength: 30
            }
        },
        // Specify validation error messages
        messages: {
            name: {
                required: "Ovo polje je obavezno",
                maxlength: "Ne smije biti više od 30 znakova"
            },
            tel: {
                required: "Ovo polje je obavezno",
                digits: "Moraju biti samo brojevi"
            },
            adress: {
                required: "Ovo polje je obavezno",
                maxlength: "Ne smije biti više od 30 znakova"
            },
            city: {
                required: "Ovo polje je obavezno",
                maxlength: "Ne smije biti više od 30 znakova"
            }

        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
            console.log("Form submitet!!!");
            $.ajax({
                type: "POST",
                url: "php/insertData.php",
                data: $('form#insertData').serialize(),
                success: function(data) {
                    console.log("success");
                    $("#table1").empty();
                    $("#table2").empty();
                    $("#table3").empty();
                    table1.destroy();
                    table2.destroy();
                    table3.destroy();
                    fillPanels();
                    $("#myModal").modal('hide');
                },
                error: function() {
                    alert("greška u spremanju");
                }
            });
        }
    });
}