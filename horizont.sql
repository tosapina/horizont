-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2017 at 12:23 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `horizont`
--

-- --------------------------------------------------------

--
-- Table structure for table `horizont`
--

CREATE TABLE `horizont` (
  `userID` int(5) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL,
  `adress` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `horizont`
--

INSERT INTO `horizont` (`userID`, `name`, `adress`, `city`, `phone`) VALUES
(1, 'Tomislav Šapina', 'Ivana Meštrovića 28', 'Piškorevci', '097 777 5479'),
(2, 'Marko Markić', 'Markićeva 33', 'Markovci', '0988885479'),
(3, 'Pero Perić', 'Perićeva 28', 'Perić Grad', '091 111 2233'),
(4, 'Željko Željkić', 'Sarajevska 33', 'Zagreb', '091 222 3311'),
(5, 'Dragan Žarković', 'Strossmayerova 66', 'Našice', '095 888 2582'),
(6, 'Đorđe Milojević', 'Sadska 33', 'Beograd', '094 444 2323'),
(7, 'Dario Srna', 'Srneća 55', 'Metković', '095 333 11 22'),
(8, 'Tihana Miševič', 'Vijenac 88', 'Osijek', '091 9034736'),
(9, 'Ružica Šapina', 'Matije Gupca 88', 'Osijek', '097 333 2255'),
(10, 'Darko Kovač', 'Ivana Meštrovića 28', 'Kuševac', '095 222 1335'),
(11, 'Antonio Đekić', 'Dobojska 22', 'Prud', '095 666 7789'),
(12, 'Pavica Došlić', 'Zorička 22', 'Vojskova', '098 555 3698'),
(13, 'Ruža Žirić', 'Zagrebačka 22', 'Karlovac', '095 444 5522'),
(14, 'Ivana Željkić', 'Lokinova 22', 'Split', '095 333 2266'),
(24, 'Tonči Huljić', 'Huljičeva 88', 'Vekaisk', '098 999 1598');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `horizont`
--
ALTER TABLE `horizont`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `horizont`
--
ALTER TABLE `horizont`
  MODIFY `userID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
