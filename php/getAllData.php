<?php 
require 'connect.php';
$con->set_charset("utf8");
$sql = "SELECT * FROM horizont";
$result = $con->query($sql);

$rows = array();
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
       		 $rows[] = $row;
    }
} 

echo json_encode( $rows, JSON_UNESCAPED_UNICODE );

?>