<?php 
require 'connect.php';

$name = strip_tags($_POST['name']);
$tel = strip_tags($_POST['tel']);
$adress = strip_tags($_POST['adress']);
$city = strip_tags($_POST['city']);

$sql = "INSERT INTO horizont (name, adress, city, phone) 
		VALUES ( 
          '{$con->real_escape_string($name)}', 
          '{$con->real_escape_string($adress)}',
          '{$con->real_escape_string($city)}',
          '{$con->real_escape_string($tel)}'
          )";

if ($con->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}


?>